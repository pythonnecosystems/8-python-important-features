# 꼭 알아야 하는 Python의 8 기능 <sup>[1](#footnote_1)</sup>
**함께 시작해요 그리고 본적이 없는 else 절과 match 구문**

## 들어가며
저자는 종종 GitHub에서 Python의 주요 프레임워크와 라이브러리 리포지토리를 탐색하곤 했다. 그렇게 함으로써 대규모 프로젝트를 어떻게 구성하는지 볼 수 있고, 회사의 솔루션 엔지니어로서 프로젝트와 프로젝트에 구현할 수 있는 새로운 것을 배울 수 있었다.

이 포스팅에서는 5~6년이라는 길지 않은 경력을 통해 배운 멋진 Python 기술 중 여러분의 프로젝트나 직장에 적용할 수 있는 여덟 가지를 공유하고자 한다.

 이 포스팅에서 다룰 내용은 아래와 같다.

- *walrus* 연산자
- `*`를 사용하여 명명된 매개변수
- union 타입(`typing.Union`이 아님)
- *`private`* 메서드와 변수
- *`flag`* 대신 *`for-else`* 사용
- *`else`* 절을 사용하되 *`if`* 절은 사용하지 않는다.
    - 전통적으로 `if-else` 구문과 연관된 `else` 블록은 `for`, `while` 및 `try` 같은 다른 제어 흐름 구조와 함께 사용할 수도 있다.
- *`if-else`* 블록과 함께 또는 대신 사용
    - 파이썬의 `or` 연산자는 `if-else` 블록을 명시적으로 사용하지 않고 조건부 검사를 간결하게 표현할 수 있는 방법이다.
- *`match`* 문
    - Python 3.10에 도입된 `match` 문은 복잡한 `if-elif-else` 또는 사전 기반 구조에 대한 보다 간결하고 가독성 높은 대안을 제공한다.

이러한 Python 기능을 배우면 더 나은 코드를 작성하고 더 능숙한 Python 프로그래머가 될 수 있다. 그리고 다음번 면접 코드 챌린지에서도 멋지게 보일 수 있으니 더할 나위 없이 좋겠죠 🏂.

## *walrus* 연산자

*walrus* 연산자는 PEP 572에 소개되었으며 다음과 같은 모습이다. 콜론 뒤에 등호/연산자를 붙인다(`:=`).

> **Note**<br>
> 이 연산자는 Python 3.8 이상에서만 사용할 수 있다.

이 연산자는 표현식의 변수에 값을 할당하는 데 사용된다(예: `NAME := expr`). 표현식은 "괄호로 묶이지 않은 튜플"을 제외하고 비교에 사용하는 것과 같은 모든 유효한 Python 표현식이 될 수 있다.

프로젝트와 동료들이 *walrus* 연산자를 사용하는 가장 일반적인 사용 사례들은 양수, `None` 또는 `False`를 반환할 수 있는 함수가 있고 그 출력에 따라 작업을 수행해야 하는 경우이다. 아래 코드는 그 예이다.

```python
def get_user(username):
    # This function fetch user info somewehere, if exists return User
    # if not return None. Let's assume that the user does not exist
    return None

def do_action(username):
    if user := get_user(username):
        pass
    else:
        print("User does not exist")
```

앞의 예에서 `username`으로 사용자를 가져오는 `get_user` 함수가 있고, `username`으로 사용자를 찾으면 `User` 객체를 출력으로 반환하고, 그렇지 않으면 `None`을 반환합니다.

`do_action` 함수에서 `get_user` 함수를 호출하면 `User` 객체 또는 `None`을 받을 수 있다는 것을 알 수 있다. 따라서 과거에 많이 사용했고 지금도 많은 사람들이 사용하는 다음과 같은 작업을 수행하는 대신 작업을 수행할 수 있다.

```python
def do_action(username):
    user = get_user(username)
    if user is not None:
        pass
    else:
        print("User does not exist")
```

간단히 `NAME := expr`을 사용하여 사용자가 있는지 여부를 `user := get_user(username)`으로 확인했다. 사용자를 얻으면 Python은 `user`에게 값을 할당하고 `if` 문 범위 내에서 이 값을 사용하여 작업을 수행할 수 있다.

이 코드를 어디에 사용하는지에 대한 또 다른 좋은 예로 다음과 같은 코드를 예로 들 수 있다.

```python
n = len(my_list)

if n > 10:
    print(f"We have {n} items in our list.")
```

*walrus* 연산자를 사용하면 코드를 다음과 같이 바꿀 수 있다.

```python
if (n := len(my_list)) > 10:
    print(f"We have {n} items in our list.")
```

다양한 사용 사례 등에 대해 더 자세히 알아보려면 [PEP 572](https://peps.python.org/pep-0572/)를 찾아 보세요.

## *`를 사용하여 명명된 매개변수
Python은 함수에 인수를 전송하는 두 가지 방법, 즉 위치와 키워드를 제공한다.

위치 인수는 함수의 각 매개변수에 대해 정의된 순서로 값을 전달한다.

```python
def double_n(n):
    return n * 2
```

이것은 간단한 예이다. 매개 변수가 하나뿐이므로 함수를 호출하면 Python은 보낸 값을 `n`에 넣는다.

```python
def full_name(first_name, last_name):
    return f"{first_name} {last_name}"
```

이 예에서는 *위치(positional)* 또는 *키워드* 매개변수 두 개가 있으며(이 중 하나를 사용하기로 결정할 수 있다), 위치별로 값이 할당되므로 인수를 보내는 순서가 출력에 영향을 미친다.

하지만 코드의 복잡성이 커지고 갑자기 많은 매개변수를 받는 함수가 생기면 상황이 까다로워진다. 이런 상황이 발생하면 위치 인수를 처리하기가 더 어려워지므로 *키워드 전용* 인수를 '강제'적으로 사용해야 할 의무가 발생한다.

그렇다면 어떻게 해야 할까? Python에서 *키워드 전용* 인수를 강제하려면 어떻게 해야 할까?

함수 정의에서 매개변수 앞에 `*` 기호를 넣기만 하면 키워드 전용 인수를 적용하는 것이다.

```python
def full_name(*, first_name, last_name):
    return f"{first_name} {last_name}"
```

따라서 `full_name`을 호출할 때마다 다음과 같이 인수를 보내야 한다.

```python
f_name = full_name(first_name="John", last_name="Doe")
```

이렇게 하면 매개변수 순서와 관련된 오류를 방지할 수 있다.

## union 타입(`typing.Union`이 아님)
Python은 정적 프로그래밍 언어(static Programming Language)가 아니지만 필요한 경우 함수 매개변수와 변수 타입을 지정할 수 있다. 그렇다고 해서 잘못된 타입을 전송하여 런타임에 발생하는 오류를 방지할 수 있는 것은 아니다.

```python
def double_n(n):
    return n * 2
```

보시다시피 이 함수는 우리가 인수로 보내는 거의 모든 것을 두 배로 반환할 수 있다. 하지만 단순화를 위해 이 함수가 있고 문자열과 정수라는 두 가지 특정 타입을 두 배로 반환한다고 가정해 보겠다.

타입 힌트를 사용하면 다른 사람이 이 코드를 사용할 때 매우 유용할 수 있다. 함수가 수신할 것으로 예상되는 값과 출력으로 반환하는 값을 명시적으로 알려준다. 또한 코드를 더 쉽게 문서화할 수 있고 IDE에서 경고를 표시하는 데 도움이 된다.

이제 `double_n` 함수를 타입을 선언해 보겠다. 이 함수는 문자열과 정수의 두 가지 타입을 두 배로 늘릴 수 있다고 했다. 따라서 이 두 가지 타입 중 하나를 기대한다고 명시적으로 말하고 수신된 타입에 따라 그 중 하나를 반환해야 한다.

내장된 `typing` 라이브러리의 `Union` 타입을 사용할 수 있다.

```python
from typing import Union

def double_n(n: Union[int, str]) -> Union[int, str]:
    return n * 2
```

이렇게 하면 함수를 호출할 때 인자로 `str`이나 `int` 타입을 인자로 보내야 하며 반환값은 인자와 동일한 타입이 될 것임을 알 수 있다.

하지만 이 코드를 작성하는 "더 간단한" 방법이 있다. 아무것도 임포트할 필요가 없다.

Python 3.10부터는 `|`를 `Union`으로 사용할 수 있으므로 `Union[int, str]`을 간단히 `int | str`으로 사용할 수 있다.

```python
def double_n(n: int | str) -> int | str:
    return n * 2
```

Python은 위의 속기 버전 사용을 권장한다. 👆 


## *`private`* 메서드와 변수
Python에서 `private` 개념은 다른 프로그래밍 언어와 다르다.

Java에서는 일부 속성이 클래스 범위 밖에서 액세스할 수 없음을 알리는 데 사용되는 private 키워드가 있다.

```java
public class Main {
    private int doubler = 2;
    
    private int double(int n) {
        return n * this.doubler;
    }
}
```

위 Java 코드는 최고의 자바 코드가 아닐 수도 있다. 하지만 `doubler` 속성과 `double` 메서드 모두에서 `private` 키워드를 살펴보자. 즉, `Main` 클래스의 범위를 벗어난 다른 곳에서는 호출할 수 없다. 그렇게 하려는 시도는 작동하지 않는다.

하지만 Python에서는 그렇지 않다. Python에는 비공개 키워드를 지원하지 않는다. 그렇다면 방금 Java에서 했던 것과 같은 작업을 어떻게 할 수 있을까?

**Python에서 두 개의 선행 밑줄로 시작하는 메서드나 변수는 비공개라는 뜻이다.**

```python
class Main:
    __doubler = 2
    
    def __double_any(self, n: int | str) -> int | str:
        return n * self.__doubler

    def double_int(n: int) -> int:
        return self.__double_any(n)
```

위의 예에서 `Main`의 범위 내에서만 호출할 수 있는 비공개 메서드 `__double_any`가 있다.

범위 밖에서 호출하려고 하면 어떻게 될까? 아래를 보자.

```python
main = Main()
main.__double_any(n=3)
```

위의 예에서처럼 호출하려고 하면 `AttributeError`가 발생한다.

**`Main` 클래스에 `__double_any`가 존재하지 않는다.**

이는 Python에서 함수 이름의 변수 이름인 '식별자'가 `__spam`(앞의 밑줄 두 개) 형식인 경우 "텍스트상"으로는 `_classname__spam`으로 대체되기 때문이다.

```python
main = Main()
main._classname__double_any(n=3)
```

다른 사람들이 하는 것을 보았고 여러 번 해본 또 다른 방법은 밑줄을 하나만 사용하여 비공개 속성을 참조하는 것이다. 이렇게 하면 여전히 속성을 비공개로 참조하지만 코드의 어느 위치에서든 액세스할 수 있다. 그러나 관례에 따라 Python 프로그래머는 `_spam` 형식의 속성(비공개임을 의미)에 액세스하거나 변경해서는 안 된다는 것을 알고 있다.

```python
class Main:
    __doubler = 2
    
    def _double_any(self, n: int | str) -> int | str:
        return n * self.__doubler

    def double_int(n: int) -> int:
        return self._double_any(n)
```


## *`flag`* 대신 *`for-else`* 사용
우리는 `for` 루프 안에서 무언가를 찾았는지 여부를 알리기 위해 플래그를 사용하곤 한다. 그게 잘못된 것은 아니다.

모든 일에는 때와 장소가 있고, 때로는 아래 예처럼 원하는 것을 찾았는지 여부에 관계없이 응답을 반환하고 싶을 때가 있다.

```python
numbers = [1, 2, 3, 4, 5]

number_6_found = False

for n in numbers:
    if n == 6:
        number_6_found = True
        break
if not number_6_found:
    print("Number six not in our list")
```

대부분이 모르는 사실 중 하나는 Python의 *for 루프*에 `else` 절이 있다는 것이다. `for`에 추가된 이 절은 루프가 정상적으로 완료된 경우에만 실행되며, 루프 내부에서 `break` 문으로 트리거되지 않는다.

```python
numbers = [1, 2, 3, 4, 6]

for n in numbers:
    if n == 6:
        # hypotetical function call
        do_something()
        break
else:
    print("I will not be executed since number six exist in our list.")
    # hypotetical function call
    no_found_report()
```


## *`else`* 블록의 강력한 기능 잠금 해제
프로그래밍과 Python을 처음 접하는 것이 아니라면 조건부 실행인 `if-else-elif`에 대해 이미 알고 있을 것이다.

그래도 간단히 정리해 보겠다.

여러분이 바구니에서 공을 집었고, 바구니에는 빨강, 초록, 파랑의 세 가지 공이 있다고 가정해 보자. 그리고 각 공마다 선물이 할당되어 있다.

![](1_H4xoIUmQZ4g66xErfQlbAw.webp)

이 경우 세 가지 결과가 나올 수 있다. 선물을 정확하게 주려면 어떤 공을 골랐는지 알아야 한다. 사람은 공을 보는 것만으로도 이 작업을 수행할 수 있다. 그러나 컴퓨터는 **조건부 실행**이라고 부르는 작업을 수행해야 한다. 컴퓨터는 고른 공을 확인하고 비교하여 색상을 확인한다.

Python과 기타 프로그래밍 언어에서는 `if-else` 블록을 사용하여 이 작업을 수행한다. 이것이 바로 Python에서 `if-else-elif`의 구조이다.

```python
if expression:
    statements
elif expression:
    statements
else:
    statements
```

그리고 이 *표현식*은 우리가 무언가를 검증하기 위해 하는 비교이다.

- 공이 파란색인가?
- 공이 초록색인가?
- 공이 빨간색인가?

*문*은 선택한 색에 따라 수행하는 작업이다. 선택한 공이 파란색이면 표현식이 포함된 `if` 블록에 속한 문이 실행된다.

```python
if ball == "blue":
    print("Ball is blue")
elif ball == "green":
    print("Ball is green")
else:
    print("If not blue, not green, ball is red")
```

예에서 볼 수 있듯이 공이 파란색과 공이 녹색에 대한 두 가지 표현식이 있다. 결국 앞의 두 표현식 중 어느 것도 아닌 경우 빨간색이어야 한다는 의미의 `else` 표현식이 있다.

하지만 조건부 실행 외에도 Python의 다른 곳에서도 `else` 블록을 사용할 수 있다는 것을 알고 있나요?

그 경우를 살펴보자.

- 이 포스팅에서 설명한 대로 **`for`** 루프
- **`while`** 루프에서
- **`try`** 문에서

### `while` 루프에서 **`else`**

**`while`** 루프에서 **`else`** 블록을 사용하면 루프의 조건이 `false`가 될 때만 실행한다.

```python
while condition:
    statements
else:
    statements
```

**for-else**와 유사하다. **`else`** 블록은 루프 내부에서 **`break`**이 트리거되지 않는 경우에만 실행된다.

책 목록에 새 책을 추가하려는 예를 살펴보겠다. 하지만 먼저 같은 제목의 책이 없는지 확인해야 한다.

```python
books = [
    {"title": "Python Distilled", "author": "Daviv M. Beazley"},
    {"title": "Fluent Python", "author": "Luciano Romalho"}
]

new_book = {"title": "Python from scracth", "author": "Yanick"}

while book in books:
    if book.get("title") == new_book.get("title"):
        # we have a book with same title, so exit the loop
        break
else:
    books.append(new_book)
```

이 예는 *flag* 변수의 사용법에 대해 설명한 [*`flag`* 대신 *`for-else`* 사용](#flag-대신-for-else-사용)를 기반으로 한다.

### `try` 문에서 **`else`**

> **Note**<br>
> 제어 흐름이 `try` 집합을 벗어나면  선택적 `else` 절이 수행된다. 즉
예외가 발생하지 않고, `return`, `continue` 또는 `break` 문이 실행되지 않은 경우 실행된다 - [Python Docs](https://docs.python.org/3/reference/compound_stmts.html#try)

즉, 코드가 문제 없이 실행되고 언급된 키워드인 `break`, `return` 또는 `continue`를 사용하여 `try` 내에서 실행을 중단하지 않는 경우에만 `else` 블록이 실행된다는 뜻이다.

```python
try:
    statements
except KeyError as e:
    raise e
else:
    statements
```

목록에서 책을 검색한다고 가정해 보자. `title`로 책을 검색하고 있으며 책을 찾으면 제거하려고 한다. 그러면 남은 책이 몇 권인지 표시되도록 한다.

```python
books = [
    {"title": "Python Distilled", "author": "Daviv M. Beazley"},
    {"title": "Fluent Python", "author": "Luciano Romalho"},
    {"title": "Python from scracth", "author": "Yanick"}
]

try:
    for book in books:
        if book.get("title") == "Python from scracth":
            books.remove(book)
except KeyError as e:
    raise e
else:
    # The code inside this block runs after successfully executing the 'try' block.
    print(f"We now have {len(books)} books left.")
    # Here, we mimic a function call or perform additional actions upon successful execution.
    update_shelve(books)
    print("Books updated successfully!")
```

## *`if-else`* 블록 대신 `or` 연산자 사용
Python은 다른 많은 프로그래밍 언어와 달리 부울 표현식을 보다 자연스럽고 인간적인 방식인 *`or`*, *`and`* 및 *`not`*으로 제공한다.


|  Boolean Expression |  Symbol (Python and Other languages)|
|---------------------|-------------------------------------|
|        and          |                &&                   | 
|        or           |                \|\|                   | 

위의 표에서 `not` 연산자를 포함하지 않은 이유는 Python에서 객체의 아이덴티티을 확인하는 역할을 하는 `==` 연산자를 객체 간의 값을 비교하는 **등호** 연산자로 사용하기 때문이다.

하지만 이 섹션에서는 *if-else* 블록을 사용하는 코드를 단순화하여 대신 `and` 연산자를 사용할 수 있다는 것을 보이고자 한다. 다음 코드를 보자.

```python
class Balls:
    __balls = ["green", "blue", "red"]
    def __init__(self, balls: list = None):
        self.balls = balls or self.__balls
```

공의 *리스트*를 매개변수로 받는 **Balls** *클래스*가 있다. 하지만 이 매개변수는 선택 사항이다. 즉, 아무것도 전송되지 않을 경우를 대비해 기본 리스트를 정의해야 한다는 뜻이다.

이를 위해 **`or`** 연산자를 사용했다. 이렇게 하면 Python이 왼쪽에 있는 표현식(`balls`)이 **`False`**, empty(*list*, *튜플*) 또는 **`None`**인지 확인한 다음 오른쪽에 있는 표현식(**`self.__balls`**)을 반환하기 때문에 작동한다. 그렇지 않으면 왼쪽에 있는 표현식인 `balls`를 반환한다.

이렇게 하면 코드의 가독성이 높아지고 유지 관리가 쉬워진다.


## *`match`* 문
**Java**나 **JavaScript** 프로그래밍의 경험이 있다면, **`switch-case`** 문에 익숙할 것이다.

**`switch`** 문은 각 **`case`**에 따라 수행해야 하는 작업이 여러 개 있을 때 유용하다.

```java
switch(expression) {
  case a:
    // do something here for a case
    break;
  case b:
    // do something here for b case
    break;
  case c:
    // do something here for c case
    break;
  case d:
    // do something here for d case
    break;
  case e:
    // do something here for e case
    break;
  default:
    // none of the previous case, do something else
}
```

위의 예에서 `switch` 내부의 *표현식*은 변수가 될 수 있으며, 입력하기 전에 한 번만 평가되고 일치하는지 여부를 찾을 때까지 각 경우와 값을 비교한다 (기본값 사용).

Python에서는 버전 3.10까지는 이와 같은 기능이 없었다. 이와 같은 여러 시나리오를 처리하려면 창의력을 발휘하여 사전을 사용하거나 중첩된 *if-elif-else* 블록을 사용해야 했다.

다행히도 이제 더 이상 이런 걱정을 할 필요가 없다. 이제 Python에 *match-case* 문이 생겼다.

앞의 예제와 유사하게 표현식을 받아 평가하고 각 `case`에 일치하는 것을 찾는다,

```python
match expression:
    case a:
        # do something here for a case
    case b:
        # do something here for b case
    case c:
        # do something here for c case
    case d:
        # do something here for d case
    case e:
        # do something here for e case
    case _:
        # none of the previous case, do something else
```

Java나 JavaScript와 달리 Python은 각 `case` 안에 `break`를 사용할 필요가 없으며, 기본 case는 `case_`를 사용하여 표시한다.

하지만 다른 프로그래밍 언어와 달리 Python의 `match` 문은 훨씬 더 강력하며 복잡한 *case*를 처리할 수 있다. 따라서 **match-case**에 대해 자세히 알아보는 글을 읽어 보아야 한다.

```python
from random import choice

balls = ["green", "blue", "red"]

match choice(balls):
    case "blue":
        print("You won a blue gift")
    case "red":
        print("You won a red gift")
    case _:
        print("You won a green gift")
```

이전 예제를 변환하여 공 리스트에서 무작위 공을 선택하고 **match-case**를 사용하여 어떤 공이 선택되었는지 확인했다.

이제 리스트에 새 공을 추가했는데 아직 선물이 할당되지 않았다고 가정해 보겠다. 하지만 리스트에 있으므로 선택할 수 있다.

Python에는 **`gurad`**라는 것이 있으며 이는 **`case`**의 일부이다. `guard`가 성공하면 케이스 블록 내부의 코드가 순서대로 실행되도록 보장하는 데 사용한다.

```python
from random import choice

balls = ["green", "blue", "red", "orange"]
guard = False

match choice(balls):
    case "blue":
        print("You won a blue gift")
    case "red":
        print("You won a red gift")
    case "orange" if guard: # it matches but the guard fails
        print("sorry! We don't have any gift for this ball yet.")
    case _:
        print("You won a green gift")
```

코드를 개선하기 위해 더 복잡하게 조합할 수 있으며, 언젠가 이에 대해 논의할 것이다.


## 마치며
보시다시피 Python은 방금 다룬 것과 같은 멋진 기능으로 가득하다. 각각의 기능을 하나의 글에서 다룰 수도 있지만, 여러분이 이해하고 Python 프로그래머로서 일상에서 사용할 수 있도록 최대한 간단하게 만들려고 노력했다.

각 주제에 대해 더 자세히 알아보고 싶다면 이 글을 작성하는 데 도움을 준 리소스를 링크하였다. 참고하세요.

- [Private Attributes](https://docs.python.org/3/tutorial/classes.html#private-variables)
- [Fluent Python](https://amzn.eu/d/0g4Eepr)
- [For-Else](https://book.pythontips.com/en/latest/for_-_else.html)
- [Walrus operator](https://realpython.com/python-walrus-operator/)
- [Python Distilled](https://amzn.eu/d/96AhbFv)
- Python official documentation on [Devdocs](https://devdocs.io/python~3.11/)
- [PEP 636 — match/case](https://peps.python.org/pep-0636/)


<a name="footnote_1">1</a>: 이 페이지는 [5 Python Features You Should Know](https://python.plainenglish.io/5-python-features-you-should-know-c3784d7f6e4e)과 [3 Python Features You Should Know — Part 2](https://yanick-andrade.medium.com/3-python-features-you-should-know-part-2-cbaef5ae773f)을 편역한 것임.
